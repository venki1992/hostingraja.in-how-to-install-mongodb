The following steps takes you to install mongoDB on HostingRaja server.
Mongo DB:

echo -e "--------------------------------"

echo -e "1. MongoDB 3.4"

echo -e "2. MongoDB 2.6"

echo -e "------------------------------
--"

echo -e "Enter your option >"

read javaversion

case "$javaversion" in



1)

touch /etc/yum.repos.d/mongodb-org.repo

echo "[mongodb-org-3.4]" >  /etc/yum.repos.d/mongodb-org.repo

echo "name=MongoDB Repository" >>  /etc/yum.repos.d/mongodb-org.repo

echo "baseurl=https://repo.mongodb.org/yum/redhat/\$releasever/mongodb-org/3.4/x86_64/" >>  /etc/yum.repos.d/mongodb-org.repo

echo "gpgcheck=1" >>  /etc/yum.repos.d/mongodb-org.repo

echo "enabled=1" >>  /etc/yum.repos.d/mongodb-org.repo

echo "gpgkey=https://www.mongodb.org/static/pgp/server-3.4.asc" >>  /etc/yum.repos.d/mongodb-org.repo

;;






2)
touch /etc/yum.repos.d/mongodb-org.repo

echo "[mongodb-org-2.6]" >  /etc/yum.repos.d/mongodb-org.repo

echo "name=MongoDB 2.6 Repository" >>  /etc/yum.repos.d/mongodb-org.repo

echo "baseurl=http://downloads-distro.mongodb.org/repo/redhat/os/x86_64/" >>  /etc/yum.repos.d/mongodb-org.repo

echo "gpgcheck=0" >>  /etc/yum.repos.d/mongodb-org.repo

echo "enabled=1" >>  /etc/yum.repos.d/mongodb-org.repo

;;
*) echo -e "You have entered wrong option. Try aftesometime"

   ;;
esac
yum -y install nodejs git  gcc-c++ make

yum -y install mongodb-org

yum -y install php-mongodb

yum -y install php-mongo

service mongod restart






Mongo DB Installation:

The above script will be helpful to install the mongoDB in our HostingRaja VPS servers. In Hostingraja we have given the Stable Version of current Version Mongo DB 3.4 and earliest version of 2.6 too. If you have used our customised control panel in VPS server then we will give the below three UI modules that should help to handle Mongo DB in the easiest way. They are 


MongoDB Database  -> Helpful to create the Database 

MongoDB Users -> This module helps to create the user for a particular database. 

MongoDB Admin -> This module helps to manage the MongoDB database and records an efficient and user-friendly way. 







Mongo DB: 


MongoDB is a document-oriented database instead of storing your data into tables.  It stores your data in collections made out of individual documents. In MongoDB, a document is a big JSON blob with no particular format or schema.


MongoDB stores data in flexible, JSON-like documents. Fields can vary from document to document and data structure can be changed over time


Ad hoc queries, indexing, and real-time aggregation provide powerful ways to access and analyze your data


MongoDB is a distributed database at its core, so high availability, horizontal scaling, and geographic distribution are built in and easy to use


MongoDB is free and open-source 




HostingRaja provides different databases to make sure the resources are flexible to the clients. To know more about the featuers offered kindly navigate to HostingRaja.in